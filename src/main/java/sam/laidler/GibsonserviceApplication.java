package sam.laidler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GibsonserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GibsonserviceApplication.class, args);
	}

}
