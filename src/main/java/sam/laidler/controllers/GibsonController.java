package sam.laidler.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/gibson-service")
public class GibsonController {
	private final String LES_PAUL = "Les Paul";
	private final String SG = "SG";

	@GetMapping(path = "/{model}")
	public String getHistory(@PathVariable("model") String model) {
		switch (model) {
		case LES_PAUL:
			return "The Gibson Les Paul is a solid body electric guitar that was first sold by the Gibson Guitar Corporation in 1952.";
		case SG:
			return "The Gibson SG is a solid-body electric guitar model introduced by Gibson in 1961 as the Gibson Les Paul SG.";
		default:
			return "Gibson Brands, Inc. (formerly Gibson Guitar Corporation) is an American manufacturer of guitars, other musical instruments, and professional audio equipment.";
		}
	}

}
